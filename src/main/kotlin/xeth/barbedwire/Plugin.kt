/*
 * Implement bukkit plugin interface
 */

package xeth.barbedwire

import org.bukkit.Bukkit
import org.bukkit.plugin.java.JavaPlugin
import java.io.File
import java.util.UUID
import org.bukkit.configuration.ConfigurationSection
import org.bukkit.configuration.file.FileConfiguration
import org.bukkit.configuration.file.YamlConfiguration
import org.bukkit.Material
import org.bukkit.entity.Player
import org.bukkit.scheduler.BukkitTask
import xeth.barbedwire.Message
import xeth.barbedwire.BarbedWireCommand

public class BarbedWire : JavaPlugin() {

    // damage value and period between damage ticks
    public var damage: Double = 1.0
    public var period: Long = 10

    // bukkit task
    private var task: BukkitTask? = null

    /**
     * Load config, update settings
     */
    public fun loadConfig() {
        // get config file
        val configFile = File(this.getDataFolder().getPath(), "config.yml")
        if ( !configFile.exists() ) {
            this.getLogger().info("No config found: generating default config.yml")
            this.saveDefaultConfig()
        }
        val config = YamlConfiguration.loadConfiguration(configFile)

        this.damage = config.getDouble("damage", this.damage)
        this.period = config.getLong("period", this.period)
    }

    /**
     * Remove previous instance, run new timer
     */
    public fun run() {
        // reload config
        this.loadConfig()

        // cancel previous task if exists
        this.task?.cancel()

        val damageAmount = this.damage
        this.task = Bukkit.getScheduler().runTaskTimer(this, object: Runnable {
            override fun run() {
                for ( player in Bukkit.getServer().getOnlinePlayers() ) {
                    val block = player.getLocation().getBlock()
                    if ( block.getType() == Material.COBWEB ) {
                        player.damage(damageAmount)
                    }
                }
            }
        }, 0, this.period)
    }

    override fun onEnable() {
        // measure load time
        val timeStart = System.currentTimeMillis()

        val logger = this.getLogger()

        // register commands
        this.getCommand("barbedwire")?.setExecutor(BarbedWireCommand(this))

        // run
        this.run()

        // print load time
        val timeEnd = System.currentTimeMillis()
        val timeLoad = timeEnd - timeStart
        logger.info("Enabled in ${timeLoad}ms")

        // print success message
        logger.info("now this is epic")
    }

    override fun onDisable() {
        logger.info("wtf i hate xeth now")
    }
}
