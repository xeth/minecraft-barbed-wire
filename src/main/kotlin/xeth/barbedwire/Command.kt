/**
 * barbed wire command, for reloading
 */

package xeth.barbedwire

import org.bukkit.entity.Player
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter

public class BarbedWireCommand(val plugin: BarbedWire) : CommandExecutor, TabCompleter {

    override fun onCommand(sender: CommandSender, cmd: Command, commandLabel: String, args: Array<String>): Boolean {    
        // no args, print plugin info
        if ( args.size == 0 ) {
            printHelp(sender)
            return true
        }

        // parse subcommand
        when ( args[0].lowercase() ) {
            "help" -> printHelp(sender)
            "reload" -> reload(sender)
            else -> { println("Invalid command, use /plugin help") }
        }

        return true
    }

    override fun onTabComplete(sender: CommandSender, command: Command, alias: String, args: Array<String>): List<String> {
        return listOf("reload")
    }

    private fun printHelp(sender: CommandSender?) {
        Message.print(sender, "[BarbedWire] by xeth for 1.16.2")
        Message.print(sender, "/barbedwire reload: reload plugin")
        return
    }

    private fun reload(sender: CommandSender?) {
        plugin.run()
        Message.print(sender, "[BarbedWire] reloaded: damage: ${plugin.damage}, period: ${plugin.period}")
    }
}