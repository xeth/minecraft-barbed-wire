/**
 * Ingame player message printing manager
 * 
 */

package xeth.barbedwire

import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.command.CommandSender
import net.kyori.adventure.text.Component

public object Message {

    public val PREFIX = "[BarbedWire]"
    public val COL_MSG = ChatColor.DARK_GREEN
    public val COL_ERROR = ChatColor.RED

    // print generic message to chat
    public fun print(sender: CommandSender?, s: String) {
        if ( sender === null ) {
            System.out.println("${PREFIX} Message called with null sender: ${s}")
            return
        }

        val msg = Component.text("${COL_MSG}${s}")
        sender.sendMessage(msg)
    }

    // print error message to chat
    public fun error(sender: CommandSender?, s: String) {
        if ( sender === null ) {
            System.out.println("${PREFIX} Message called with null sender: ${s}")
            return
        }

        val msg = Component.text("${COL_ERROR}${s}")
        sender.sendMessage(msg)
    }
}